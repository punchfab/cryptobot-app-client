package com.gplux.cryptobotpro

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gplux.cryptobotpro.adapter.SearchSymbolAdapter
import com.gplux.cryptobotpro.adapter.TradeAdapter
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.Network
import com.gplux.cryptobotpro.network.RetrofitClient
import com.mancj.materialsearchbar.MaterialSearchBar
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import java.net.URISyntaxException


class MainActivity : AppCompatActivity(), MaterialSearchBar.OnSearchActionListener {

    private lateinit var mSocket: Socket
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var tradeList: MutableList<Trade>
    private var result = ArrayList<String>()
    private var coin = ArrayList<Symbol>()
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseMessaging.getInstance().subscribeToTopic("crypto_pro")
        sharedPref = getSharedPreferences("CPB", Context.MODE_PRIVATE)
        val network = Network(applicationContext, sharedPref)
        network.login()

        btnFilter.setOnClickListener {
            val intent = Intent(applicationContext, SortSymbolActivity::class.java)
            startActivity(intent)
        }

        rvTrades.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        rvTrades.layoutManager = layoutManager
        rvTrades.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))
        tradeList = mutableListOf()
        rvTrades.adapter = TradeAdapter(applicationContext, tradeList, sharedPref)

        try {
            mSocket = IO.socket("${RetrofitClient.url}:${RetrofitClient.port0}")
            mSocket.connect()
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        mSocket.emit("main")
        mSocket.on("updateTrades") { args ->
            runOnUiThread(Runnable {
                val data = args[0] as String
                val gson = Gson()
                tradeList.clear()
                try {
                    val arrayTutorialType = object : TypeToken<List<Trade>>() {}.type
                    tradeList = gson.fromJson(data, arrayTutorialType)
                    rvTrades.adapter = TradeAdapter(applicationContext, tradeList, sharedPref)

                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }


        network.symbol{ list ->
            coin.addAll(list)
            for (coin in list)
                result.add(coin.symbol)

        }

        val adapter = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, result)
        search_spinner.adapter = adapter
        val sbAdapter = SearchSymbolAdapter(applicationContext, coin, sharedPref)
        listSearch.adapter = sbAdapter

        searchBar.addTextChangeListener(object : TextWatcher {
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //SEARCH FILTER
                sbAdapter.getFilter().filter(charSequence)
            }

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (coin.size == 0) {
                    network.symbol{ list ->
                        coin.addAll(list)
                        for (coin in list) result.add(coin.symbol)
                    }
                    Toast.makeText(applicationContext, coin.size.toString(), Toast.LENGTH_LONG).show()
                }
            }
            override fun afterTextChanged(editable: Editable) {}
        })



        searchBar.setOnSearchActionListener(object: MaterialSearchBar.OnSearchActionListener{
            override fun onButtonClicked(buttonCode: Int) {
                Toast.makeText(applicationContext,buttonCode.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onSearchStateChanged(enabled: Boolean) {
                when (enabled) {
                    true -> {
                        listSearch.visibility = LinearLayout.VISIBLE
                        rvTrades.visibility = LinearLayout.GONE
                    }

                    false -> {
                        listSearch.visibility = LinearLayout.GONE
                        rvTrades.visibility = LinearLayout.VISIBLE
                    }
                }
            }

            override fun onSearchConfirmed(text: CharSequence?) {}
        })

        /*network.trades{ list ->
            var array = ArrayList<Trade>(list)
            rvTrades.adapter = TradeAdapter(applicationContext, array)
        }*/
    }

    override fun onDestroy() {
        super.onDestroy()
        mSocket.off ( "updateTrades")
        mSocket.off ( "main")
        //Toast.makeText(applicationContext,"Destroy disconnect",Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        mSocket.off ( "updateTrades")
        mSocket.off ( "main")
        //Toast.makeText(applicationContext,"Stop disconnect",Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        //val network = Network(applicationContext)
        mSocket.emit("main")
        mSocket.on("updateTrades") { args ->
            runOnUiThread(Runnable {
                val data = args[0] as String
                val gson = Gson()
                tradeList.clear()
                try {
                    val arrayTutorialType = object : TypeToken<List<Trade>>() {}.type
                    tradeList = gson.fromJson(data, arrayTutorialType)
                    rvTrades.adapter = TradeAdapter(applicationContext, tradeList, sharedPref)

                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }

        //Toast.makeText(applicationContext, search_spinner.selectedItemPosition.toString(), Toast.LENGTH_LONG).show()
    }


    override fun onButtonClicked(buttonCode: Int) {

    }

    override fun onSearchStateChanged(enabled: Boolean) {}

    override fun onSearchConfirmed(text: CharSequence?) {}

}
