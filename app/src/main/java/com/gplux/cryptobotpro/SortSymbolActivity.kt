package com.gplux.cryptobotpro

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gplux.cryptobotpro.adapter.SortSymbolAdapter
import com.gplux.cryptobotpro.adapter.TradeAdapter
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.RetrofitClient
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sort_symbol.*
import kotlinx.android.synthetic.main.activity_trade.*
import org.json.JSONException
import org.json.JSONObject
import java.net.URISyntaxException

class SortSymbolActivity : AppCompatActivity() {

    private lateinit var mSocket: Socket
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var symbolList: ArrayList<Symbol>
    private val arrayPair = arrayListOf("BTC", "ETH", "USDT", "BNB")
    private val arrayPeriod = arrayListOf("1m","3m","5m","15m","30m","1h","2h","4h","6h","8h","12h","1d","3d","1w","1M")
    private val arrayPercent = arrayListOf("All", "(+)", "(-)")
    private var sortParams = "false"
    private var currentCoin = arrayPair[0]
    private var newCoin = arrayPair[0]
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sort_symbol)

        sharedPref = getSharedPreferences("CPB", Context.MODE_PRIVATE)
        val adapter0 = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayPair)
        val adapter1 = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayPeriod)
        val adapter2 = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayPercent)

        spPair.adapter = adapter0
        spPeriod.adapter = adapter1
        spPercent.adapter = adapter2
        spPair.setSelection(0)
        spPeriod.setSelection(11)
        spPercent.setSelection(0)

        rvSortSymbol.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        rvSortSymbol.layoutManager = layoutManager
        rvSortSymbol.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))
        symbolList = arrayListOf()
        var sortAdapter = SortSymbolAdapter(applicationContext, symbolList, sharedPref)
        rvSortSymbol.adapter = sortAdapter

        try {
            mSocket = IO.socket("${RetrofitClient.url}:${RetrofitClient.port0}")
            mSocket.connect()
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        val obj = JSONObject()
        obj.put("symbol", arrayPair[spPair.selectedItemPosition])
        obj.put("period", arrayPeriod[spPeriod.selectedItemPosition])
        obj.put("percent", arrayPercent[spPercent.selectedItemPosition].toString())
        obj.put("sort", sortParams)
        obj.put("currentArray", "[]")

        mSocket.emit("sort", obj)
        mSocket.on("filters") { args ->
            runOnUiThread(Runnable {
                val data = args[0] as String
                val gson = Gson()
                //symbolList.clear()
                try {
                    val arrayTutorialType = object : TypeToken<List<Symbol>>() {}.type
                    //symbolList = gson.fromJson(data, arrayTutorialType)
                    //rvSortSymbol.adapter = SortSymbolAdapter(applicationContext, symbolList)
                    sortAdapter.update(gson.fromJson(data, arrayTutorialType))
                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }

        val slHandler = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                newCoin = arrayPair[spPair.selectedItemPosition]
                sendSort()
            }
        }

        spPair.onItemSelectedListener = slHandler
        spPercent.onItemSelectedListener = slHandler
        spPeriod.onItemSelectedListener = slHandler

        colPair.setOnClickListener{
            sortParams = when (sortParams) {
                "false" -> "pair_desc"
                "pair_desc" -> "pair_asc"
                else -> "pair_desc"
            }
            sendSort()
            imVisibility(sortParams, ivPair)
        }

        colChg.setOnClickListener{
            sortParams = when (sortParams) {
                "false" -> "chg_desc"
                "chg_desc" -> "chg_asc"
                else -> "chg_desc"
            }
            sendSort()
            imVisibility(sortParams, ivChg)
        }
        colVolume.setOnClickListener{
            sortParams = when (sortParams) {
                "false" -> "vol_desc"
                "vol_desc" -> "vol_asc"
                else -> "vol_desc"
            }
            sendSort()
            imVisibility(sortParams, ivVol)
        }
        colTrades.setOnClickListener{
            sortParams = when (sortParams) {
                "false" -> "tr_desc"
                "tr_desc" -> "tr_asc"
                else -> "tr_desc"
            }
            sendSort()
            imVisibility(sortParams, ivTrades)
        }

        btnMain.setOnClickListener {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //mSocket.disconnect()
        mSocket.emit("offSort")
        mSocket.off( "sort")
        mSocket.off( "filters")
        Toast.makeText(applicationContext,"sort destroy", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        //mSocket.disconnect()
        mSocket.emit("offSort")
        //mSocket.off( "sort")
        //mSocket.off( "filters")
        Toast.makeText(applicationContext,"sort disconnect", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        sendSort()
        Toast.makeText(applicationContext,"sort resume", Toast.LENGTH_SHORT).show()
        //mSocket.emit("sort", obj)
    }

    fun sendSort () {
        val obj = JSONObject()
        val gson = Gson()
        var jsonArray = gson.toJson(symbolList)
        if (currentCoin != newCoin) {
            jsonArray = "[]"
            currentCoin = newCoin
        }
        obj.put("symbol", arrayPair[spPair.selectedItemPosition])
        obj.put("period", arrayPeriod[spPeriod.selectedItemPosition])
        obj.put("percent", spPercent.selectedItemPosition.toString())
        obj.put("sort", sortParams)
        obj.put("currentArray", jsonArray)
        //symbolList

        //mSocket.disconnect()
        //mSocket.emit("offSort")
        //mSocket.off( "sort")
        //mSocket.off( "filters")
        mSocket.emit("sort", obj)
    }

    private fun imVisibility (col: String, iv: ImageView) {
        var params = col.split("_")
        ivPair.visibility = if (params[0] == "pair") LinearLayout.VISIBLE else LinearLayout.GONE
        ivChg.visibility = if (params[0] == "chg") LinearLayout.VISIBLE else LinearLayout.GONE
        ivTrades.visibility = if (params[0] == "tr") LinearLayout.VISIBLE else LinearLayout.GONE
        ivVol.visibility = if (params[0] == "vol") LinearLayout.VISIBLE else LinearLayout.GONE

        when(params[1]) {
            "desc" -> iv.setImageResource(R.drawable.ic_vertical_align_bottom_black_24dp)
            "asc" -> iv.setImageResource(R.drawable.ic_vertical_align_top_black_24dp)
        }
    }

    //sp periodo
    //sp porcentaje
    //sp par
    //tabla symbol,percent,#trades,volume
}
