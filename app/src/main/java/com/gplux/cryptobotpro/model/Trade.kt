package com.gplux.cryptobotpro.model

import android.graphics.Color
import android.widget.EditText
import android.widget.TextView
import com.google.gson.annotations.SerializedName
import kotlin.math.pow

class Trade {
    @SerializedName("symbol")
    val symbol = ""

    @SerializedName("status")
    val status = ""

    @SerializedName("market")
    var market = ""

    @SerializedName("leverage")
    var leverage = ""

    @SerializedName("positionSide")
    var positionSide = ""

    @SerializedName("price")
    var price: Price? = null

    @SerializedName("buy")
    var buy: Buy? = null

    @SerializedName("sell")
    var sell: Sell? = null

    class Price {
        @SerializedName("entry")
        val entry = ""
        @SerializedName("exit")
        val exit = ""
    }

    class Buy {
        @SerializedName("active")
        val active: String = ""

        @SerializedName("limit")
        val limit: String = ""

        @SerializedName("amount")
        val amount: String = ""

        @SerializedName("order")
        val order: Order? = null

        class Order {
            @SerializedName("status")
            val status = ""
        }
    }

    class Sell {
        @SerializedName("active")
        val active: String = ""
        @SerializedName("type")
        val type: String = ""
        @SerializedName("limit")
        val limit: String = ""
        @SerializedName("stop")
        val stop: String = ""
        @SerializedName("stop_limit")
        val stopLimit: String = ""
        @SerializedName("amount")
        val amount: String = ""

        @SerializedName("order")
        val order: Order? = null

        class Order {
            @SerializedName("oco0")
            var oco0: Oco0? = null
            @SerializedName("oco1")
            var oco1: Oco1? = null

            @SerializedName("status")
            var status = ""

            class Oco0 {
                @SerializedName("status")
                var status = ""
            }

            class Oco1 {
                @SerializedName("status")
                var status = ""
            }
        }
    }

    fun percentage(x: Double, y: Double): String {
        var lastPer: Double = (y - x) * 100 / x
        return String.format("%.2f", lastPer)
    }

    fun resultPer(x: Double, percentage: Double, digits: Int = 8): String {
        var result: Double = x + x * (percentage / 100)
        return String.format("%.${digits}f", result)
    }

    fun setColorPer (per: Any, s: TextView) {
        var percent =
            if (per::class.java.name == "android.widget.TextView") per as TextView
            else per as TextView
        when {
            percent.text.toString().toDouble() > 0 -> {
                percent.setTextColor(Color.rgb(5,195,13))
                s.setTextColor(Color.rgb(5,195,13))
            }
            percent.text.toString().toDouble() < 0 -> {
                percent.setTextColor(Color.RED)
                s.setTextColor(Color.RED)
            }
            else -> {
                percent.setTextColor(Color.GRAY)
                s.setTextColor(Color.GRAY)
            }
        }
    }

    fun formatDigits(value: String, x: Int = 8): String {
        //var digits = Math.pow(10.0,x.toDouble()).toInt()
        var v = 0.0
        if (value.isNotEmpty()) v = value.toDouble()
        var price = v
        return String.format("%.${x}f", price)
    }

    fun qtyCalc (price: String, balance: String, dec: Int = 8): String {
        var qty = balance.toDouble().div(price.toDouble())
        qty = formatDigits(qty.toString(), dec).toDouble()
        var v = qty * price.toDouble()

        while (v > balance.toDouble()) {
            qty = minusPrice(qty.toString(), dec).toDouble()
            v = qty * price.toDouble()
        }

        return qty.toString()

    }

    fun perCalc (pBuy: String, pSell: String, percent: String): String {
        var percentage = percentage(pBuy.toDouble(), pSell.toDouble())

        while (percentage.toDouble() < percent.toDouble()) percentage = plusPrice(percentage, 2)

        return  percentage
    }

    fun plusPrice(value: String, x: Int = 8): String {
        var digits = 10.0.pow(x.toDouble()).toInt()
        var v = 0.0
        if (value.isNotEmpty()) v = value.toDouble()

        var price = v * digits
        price = (price.plus(1)).div(digits)
        return String.format("%.${x}f", price)
    }

    fun minusPrice(value: String, x: Int = 8, m: Boolean = false): String {
        var digits = Math.pow(10.0,x.toDouble()).toInt()
        var v = 0.0
        if (value.isNotEmpty()) v = value.toDouble()
        var price = v * digits

        if (price > 0 && !m || m) {
            price = (price.minus(1)).div(digits)
        }
        return String.format("%.${x}f", price)
    }
}