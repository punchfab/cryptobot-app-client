package com.gplux.cryptobotpro.model

import com.google.gson.annotations.SerializedName

class Balance {
    @SerializedName("available")
    var available: String = "0.00"
    @SerializedName("onOrder")
    var onOrder: String = "0.00"
    @SerializedName("asset")
    var coin: String? = null
}