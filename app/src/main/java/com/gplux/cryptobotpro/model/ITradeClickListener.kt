package com.gplux.cryptobotpro.model

import android.view.View

interface ITradeClickListener {
    fun onTradeClick(view: View, position: Int)
}