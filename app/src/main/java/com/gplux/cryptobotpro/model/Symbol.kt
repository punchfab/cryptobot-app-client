package com.gplux.cryptobotpro.model

import com.google.gson.annotations.SerializedName

class Symbol {
    @SerializedName(value="symbol", alternate= ["pair"])
    val symbol = ""
    var active = ""
    var priceChange = ""
    @SerializedName(value="priceChangePercent", alternate= ["percentChange"])
    var priceChangePercent = ""
        get() {
            return if(field.isNotEmpty()) Trade().formatDigits(field,2) else field
        }
    @SerializedName(value="lastPrice", alternate= ["close"])
    var lastPrice = ""
    @SerializedName("digits")
    val digits = ""
    @SerializedName("dec_qty")
    val dec_qty = ""
    @SerializedName(value="assetVolume", alternate = ["quoteVolume"])
    var volume = ""
        get() {
            return if(field.isNotEmpty()) Trade().formatDigits(field,2) else field
        }
    @SerializedName(value="trades", alternate = ["numTrades"])
    val trades = ""
    val spread = ""


}