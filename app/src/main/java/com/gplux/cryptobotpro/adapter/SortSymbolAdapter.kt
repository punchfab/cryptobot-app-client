package com.gplux.cryptobotpro.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.gplux.cryptobotpro.R
import com.gplux.cryptobotpro.TradeActivity
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.Network
import kotlinx.android.synthetic.main.symbol_layout.view.*

class SortSymbolAdapter(private var context: Context, private var symbolsList:ArrayList<Symbol>, private val shaPref: SharedPreferences):
    RecyclerView.Adapter<SortSymbolAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.sort_symbol_layout, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return symbolsList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.pair.text = symbolsList[position].symbol
        holder.change.text = symbolsList[position].priceChangePercent
        Trade().setColorPer(holder.change, holder.sign )
        holder.volume.text = symbolsList[position].volume
        holder.nTrades.text = symbolsList[position].trades

        val network = Network(context, shaPref)
        var item = symbolsList[position]
        holder.itemView.setOnClickListener {
            Toast.makeText(context,"item",Toast.LENGTH_SHORT).show()
            network.digs(item.symbol) { dec ->
                val intent = Intent(context, TradeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("symbol", item.symbol)
                intent.putExtra("price", item.lastPrice)
                intent.putExtra("percent", item.priceChangePercent)
                intent.putExtra("digits", dec!!.digits)
                intent.putExtra("decQty", dec!!.dec_qty)
                context.startActivity(intent)
            }
        }
    }

    fun update (items: List<Symbol>) {
        if (items != null && items.isNotEmpty()) {
            /*for (item in items) {
                val index = symbolsList.indexOfFirst { it.symbol == item.symbol }
                if (index == -1) symbolsList.add(item)
                else symbolsList[index] = item
            }*/
            symbolsList.clear()
            symbolsList.addAll(items)
            notifyDataSetChanged()
        }
    }

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        internal var pair: TextView = itemView.findViewById(R.id.tvsSymbol) as TextView
        internal var change: TextView = itemView.findViewById(R.id.tvsPercent) as TextView
        internal var volume: TextView = itemView.findViewById(R.id.tvsVolume) as TextView
        internal var nTrades: TextView = itemView.findViewById(R.id.tvsTrades) as TextView
        internal var sign: TextView = itemView.findViewById(R.id.tvsSign) as TextView

        //override fun onClick(p0: View?) {}
    }
}
