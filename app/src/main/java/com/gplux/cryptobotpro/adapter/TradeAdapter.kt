package com.gplux.cryptobotpro.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.gplux.cryptobotpro.R
import com.gplux.cryptobotpro.TradeActivity
import com.gplux.cryptobotpro.model.ITradeClickListener
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.Network

class TradeAdapter(private var context: Context, private var tradeList:List<Trade>, private val shaPref: SharedPreferences):
    RecyclerView.Adapter<TradeAdapter.MyViewHolder>() {

    override fun getItemCount(): Int {
        return tradeList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var entry = tradeList[position].price!!.entry
        var exit = tradeList[position].price!!.exit
        var statusBuy = tradeList[position].buy!!.order
        var statusSell = tradeList[position].sell!!.order
        var perc = if (entry.isNotEmpty() && exit.isNotEmpty()) Trade().percentage(entry.toDouble(),exit.toDouble()) else "0.00"
        var resultName = if (perc.toDouble() < 0) "Loss:" else "Profit:"
        val network = Network(context, shaPref)
        var price = if (entry.isNullOrEmpty()) exit else entry
        holder.symbol.text = tradeList[position].symbol
        holder.status.text = tradeList[position].status
        holder.entry.text = entry
        holder.exit.text = exit
        holder.resultName.text = resultName
        fun resultTr () =
            if (tradeList[position].positionSide == "long") perc.toDouble() * tradeList[position].leverage.toInt()
            else perc.toDouble() * tradeList[position].leverage.toInt() * (-1)
        holder.resultTrade.text = if (tradeList[position].market == "futures") String.format("%.2f", resultTr()) else perc
        Trade().setColorPer(holder.resultTrade, holder.signPercent)
        holder.statusBuy.text = if (statusBuy == null) "NONE" else statusBuy!!.status
        holder.statusSell.text =
        when {
            statusSell == null -> "NONE"
            statusSell.oco0 != null -> {
                if (statusSell.oco0!!.status == "FILLED" || statusSell.oco1!!.status == "FILLED") "FILLED"
                else "NEW"
            }
            else -> statusSell!!.status
        }

        /*if (position % 2 == 0)
            holder.root_view.setCardBackgroundColor(Color.parseColor("#E1E1E1"))*/

        holder.setClick(object: ITradeClickListener{
            override fun onTradeClick(view: View, position: Int) {
                //Toast.makeText(context, tradeList[position].symbol, Toast.LENGTH_SHORT).show()
            }
        })

        holder.itemView.setOnClickListener {

            network.digs(tradeList[position].symbol){ dec ->
                val intent = Intent(context, TradeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("symbol", holder.symbol.text.toString())
                intent.putExtra("price", price)
                intent.putExtra("percent", "0.00")
                intent.putExtra("digits", dec!!.digits)
                intent.putExtra("decQty", dec!!.dec_qty)
                context.startActivity(intent)
            }

            //Toast.makeText(context, tradeList[position].symbol, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.trade_layout, parent, false)
        return MyViewHolder(itemView)
    }


    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        //internal var root_view: CardView
        internal var symbol: TextView
        internal var status: TextView
        internal var entry: TextView
        internal var exit: TextView
        internal var resultName: TextView
        internal var resultTrade: TextView
        internal var signPercent: TextView
        internal var statusBuy: TextView
        internal var statusSell: TextView

        internal lateinit var tradeClickListener: ITradeClickListener

        fun setClick(transactionClickListener: ITradeClickListener)
        {
            this.tradeClickListener = transactionClickListener
        }

        init {
            //root_view = itemView.findViewById(R.id.root_View) as CardView
            symbol = itemView.findViewById(R.id.tSymbol) as TextView
            status = itemView.findViewById(R.id.tStatus) as TextView
            entry = itemView.findViewById(R.id.tEntryPrice) as TextView
            exit = itemView.findViewById(R.id.tExitPrice) as TextView
            resultName = itemView.findViewById(R.id.tResultTrade) as TextView
            resultTrade = itemView.findViewById(R.id.tPercentResult) as TextView
            statusBuy = itemView.findViewById(R.id.tStatusBuy) as TextView
            statusSell = itemView.findViewById(R.id.tStatusSell) as TextView
            signPercent = itemView.findViewById(R.id.tSignResult) as TextView
        }

        override fun onClick(p0: View?) {
            tradeClickListener.onTradeClick(p0!!, adapterPosition)
        }

    }
}