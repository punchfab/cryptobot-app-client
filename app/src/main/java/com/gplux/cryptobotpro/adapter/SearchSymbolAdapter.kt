package com.gplux.cryptobotpro.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.gplux.cryptobotpro.R
import com.gplux.cryptobotpro.TradeActivity
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.Network
import kotlinx.android.synthetic.main.symbol_layout.view.*
import kotlin.collections.ArrayList

class SearchSymbolAdapter(context: Context, private var symbolList: ArrayList<Symbol>, private val shaPref: SharedPreferences):
    ArrayAdapter<Symbol>(context, R.layout.symbol_layout, symbolList), Filterable {

    var filterListSymbol: ArrayList<Symbol> = symbolList
    var filter: CustomFilter? = null

    override fun getCount(): Int {return symbolList.size}
    override fun getItem(position: Int): Symbol? {return symbolList[position]}
    override fun getItemId(position: Int): Long {return  symbolList.indexOf(getItem(position)).toLong() }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val viewHolder = MyViewHolder()

        //if (view == null) {
        view = LayoutInflater.from(context).inflate(R.layout.symbol_layout, parent, false)

        //viewHolder = MyViewHolder()
        viewHolder.symbol = view!!.findViewById(R.id.sSymbol) as TextView
        viewHolder.lastPrice = view!!.findViewById(R.id.sPrice) as TextView
        viewHolder.priceChangePercent = view!!.findViewById(R.id.sPercent) as TextView
        //}

        val item = getItem(position)

        val network = Network(context, shaPref)
        network.digs(item!!.symbol){result ->

            if (result != null) {
                viewHolder.lastPrice!!.text = Trade().formatDigits(item!!.lastPrice, result!!.digits.toInt())
            } else {
                viewHolder.lastPrice!!.text = item!!.lastPrice
            }
            viewHolder.symbol!!.text = item!!.symbol
            viewHolder.priceChangePercent!!.text = Trade().formatDigits(item!!.priceChangePercent, 2)
            Trade().setColorPer(viewHolder.priceChangePercent!!, view.sSignPercent )

        }

        view.setOnClickListener {
            network.digs(item.symbol) { dec ->
                val intent = Intent(context, TradeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("symbol", item.symbol)
                intent.putExtra("price", item.lastPrice)
                intent.putExtra("percent", item.priceChangePercent)
                intent.putExtra("digits", dec!!.digits)
                intent.putExtra("decQty", dec!!.dec_qty)
                context.startActivity(intent)
            }
        }

        return view
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = CustomFilter()
        }

        return filter as CustomFilter
    }

    inner class MyViewHolder {
        internal var symbol: TextView? = null
        internal var priceChangePercent: TextView? = null
        internal var lastPrice: TextView? = null
    }

    inner class CustomFilter: Filter() {

        override fun performFiltering(p0: CharSequence?): FilterResults {
            var results = FilterResults()
            var constraint = p0

            if (p0 != null && p0.isNotEmpty()) {
                constraint = p0.toString().toUpperCase()

                var filters = ArrayList<Symbol>()

                for (list in filterListSymbol) {
                    if (list.symbol.toUpperCase().contains(constraint)) {
                        filters.add(list)
                    }
                }
                results.count = filters.size
                results.values = filters
            } else {
                results.count = filterListSymbol.size
                results.values = filterListSymbol
            }

            return results
        }

        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
            symbolList = p1!!.values as ArrayList<Symbol>
            notifyDataSetChanged()
        }

    }
}