package com.gplux.cryptobotpro.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    const val url = "http://134.122.124.15"
    //const val url = "http://192.168.100.10"
    //const val url = "http://192.168.1.7"
    const val port0 = "3000"
    const val port1 = "3030"
    //192.168.1.7
    //192.168.100.10
    val instance: RetrofitInterface by lazy{
        val retrofit = Retrofit.Builder()
            .baseUrl("$url:$port0")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //GitHubService service = retrofit.create(GitHubService.class);
        retrofit.create(RetrofitInterface::class.java)
    }
}

