package com.gplux.cryptobotpro.network

import com.gplux.cryptobotpro.model.Balance
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import retrofit2.Call
import retrofit2.http.*

interface RetrofitInterface {
    @GET("prev")
    fun getSymbol(@Header("x-access-token") token: String): Call<List<Symbol>>

    @GET("balance/{symbol}")
    fun getBalance(@Header("x-access-token") token: String, @Path("symbol") symbol: String) : Call<List<Balance>>

    @GET("digs/{symbol}")
    fun getDigs(@Header("x-access-token") token: String, @Path("symbol") symbol: String) : Call<Symbol>

    @GET("/balance_order/{coin}")
    fun getBalCoin(@Header("x-access-token") token: String, @Path("coin") coin: String) : Call<Balance>

    @FormUrlEncoded
    @POST("/set_order")
    fun setOrder(
        @Header("x-access-token") token: String,
        @Field("symbol") symbol: String,
        @Field("type") type: Int,
        @Field("buy_active") buy_active: Boolean,
        @Field("sell_active") sell_active: Boolean,
        @Field("buy_limit") buy_limit: String = "",
        @Field("buy_amount") buy_amount: String = "",
        @Field("sell_limit") sell_limit: String = "",
        @Field("sell_stop") sell_stop: String = "",
        @Field("sell_stop_limit") sell_stop_limit: String = "",
        @Field("sell_amount") sell_amount: String = ""
    ): Call<String>

    @GET("/order_cancel/{symbol}")
    fun orderCancel(
        @Header("x-access-token") token: String,
        @Path("symbol") symbol:String) : Call<String>

    @GET("/trades")
    fun getTrades(@Header("x-access-token") token: String): Call<List<Trade>>

    @GET("/trade/{symbol}")
    fun getTrade(@Header("x-access-token") token: String, @Path("symbol") symbol: String): Call<Trade>

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("email") email: String = "sales.gameplux@gmail.com",
        @Field("password") password: String = "gplX8812fhi5"
    ): Call<String>

}