package com.gplux.cryptobotpro.network

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import android.widget.Toast
import com.gplux.cryptobotpro.TradeActivity
import com.gplux.cryptobotpro.model.Balance
import com.gplux.cryptobotpro.model.Symbol
import com.gplux.cryptobotpro.model.Trade
import kotlinx.android.synthetic.main.activity_trade.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Network(val context: Context, private val shaPref: SharedPreferences) {

    private val rt = RetrofitClient.instance
    private var token: String = shaPref.getString("auth","").toString()
    fun digs(symbol: String, body: (Symbol?) -> Unit) {
        rt.getDigs(token, symbol)
            .enqueue(object: Callback<Symbol> {
                override fun onFailure(call: Call<Symbol>, t: Throwable) {
                    Toast.makeText(context, "digs "+t.message, Toast.LENGTH_LONG).show()
                }
                override fun onResponse(call: Call<Symbol>, response: Response<Symbol>) {
                    val result = response.body()
                    body(result)
                }
            })
    }

    fun symbol(body: (List<Symbol>) -> Unit) {
        if (shaPref.contains("auth") && token.isNotEmpty()) {
            rt.getSymbol(token)
                .enqueue(object: Callback<List<Symbol>> {
                    override fun onFailure(call: Call<List<Symbol>>, t: Throwable) {
                        Toast.makeText(context, "symbol "+t.message, Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(call: Call<List<Symbol>>, response: Response<List<Symbol>>) {

                        val result = response.body()!!
                        body(result)
                    }

                })
        } else Toast.makeText(context, "Error token", Toast.LENGTH_LONG).show()

    }

    fun balance(symbol: String, body: (List<Balance>) -> Unit) {
        rt.getBalance(token, symbol)
            .enqueue(object: Callback<List<Balance>> {
                override fun onFailure(call: Call<List<Balance>>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<List<Balance>>, response: Response<List<Balance>>) {
                    val result = response.body()!!
                    body(result)
                }
            })
    }

    fun orderCancel(symbol: String, body: (String) -> Unit) {
        rt.orderCancel(token, symbol)
            .enqueue(object: Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    val result = response.body()!!
                    body(result)
                    Toast.makeText(context, response.body()!!, Toast.LENGTH_LONG).show()
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

    fun trades(body: (List<Trade>) -> Unit) {
        rt.getTrades(token)
            .enqueue(object : Callback<List<Trade>> {
                override fun onResponse(call: Call<List<Trade>>, response: Response<List<Trade>>) {
                    val result = response.body()!!
                    body(result)
                }

                override fun onFailure(call: Call<List<Trade>>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

    fun trade(symbol: String, body: (Response<Trade>) -> Unit) {
        rt.getTrade(token, symbol)
            .enqueue(object : Callback<Trade> {
                override fun onResponse(call: Call<Trade>, response: Response<Trade>) {
                    //val result = response
                    body(response)
                }

                override fun onFailure(call: Call<Trade>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

    fun login() {
        val editor = shaPref.edit()
        rt.login()
            .enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()}
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    val obj = JSONObject(response.body()!!)
                    editor.putString("auth", obj.getString("token"))
                    editor.commit()
                    //Toast.makeText(context, obj.getString("token"), Toast.LENGTH_LONG).show()
                }
            })
    }
}