package com.gplux.cryptobotpro

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.gplux.cryptobotpro.network.RetrofitClient
import kotlinx.android.synthetic.main.activity_trade.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.net.URISyntaxException
import android.os.Handler
import android.text.InputFilter
import androidx.core.widget.addTextChangedListener
import retrofit2.Callback
import com.gplux.cryptobotpro.model.Trade
import com.gplux.cryptobotpro.network.Network
import kotlinx.android.synthetic.main.activity_trade.view.*


class TradeActivity : AppCompatActivity() {

    private lateinit var mSocket: Socket
    private val typeOrder = arrayListOf("Limit", "Stop-Limit", "OCO")
    private  val rulesTotal = "{\"BTC\":\"0.0001\",\"BNB\":\"0.1\",\"ETH\":\"0.01\",\"USDT\":\"10\",\"TRON\":\"100\",\"XRP\":\"10\"}"
    private var balance = String()
    private var bCoin = String()
    private var decQty = 0
    private lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade)

        sharedPref = getSharedPreferences("CPB", Context.MODE_PRIVATE)
        val adapterOrders = ArrayAdapter(this, android.R.layout.simple_spinner_item, typeOrder)
        spTypeOrderSell.adapter = adapterOrders
        val network = Network(applicationContext, sharedPref)

        spTypeOrderSell.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        addLimitContent()
                    }
                    1 -> {
                        addStopContent()
                    }
                    2 -> {
                        addOCOContent()
                    }
                }
                //Toast.makeText(applicationContext, position.toString(), Toast.LENGTH_SHORT).show()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val bundle:Bundle = intent.extras!!
        val symbol = bundle.get("symbol")
        val price = bundle.get("price")
        var digits = bundle.get("digits").toString().toInt()
        decQty = bundle.get("decQty").toString().toInt()
        var percent = bundle.get("percent").toString()

        tvSymbol.text = symbol.toString()
        tvLastPrice.text = String.format("%.${digits}f",price.toString().toDouble())
        var priceMaxLength = arrayOf(InputFilter.LengthFilter(tvLastPrice.text.toString().length))
        //Toast.makeText(applicationContext, tvLastPrice.text.toString().length.toString(), Toast.LENGTH_LONG).show()
        tvPercentChange.text = String.format("%.2f", percent.toDouble())
        etPriceLimit.filters = priceMaxLength
        etPriceLimit.setText(Trade().formatDigits(price.toString(), digits))
        etSellStopLimit.filters = priceMaxLength
        etSellStopLimit.setText(Trade().formatDigits(price.toString(), digits))
        etSellStop.filters = priceMaxLength
        etSellStop.setText(Trade().formatDigits(price.toString(), digits))
        etPriceSell.filters = priceMaxLength
        etPriceSell.setText(Trade().formatDigits(price.toString(), digits))

        buyEnabled(swBuyActive.isChecked)
        swBuyActive.setOnCheckedChangeListener { _, b -> buyEnabled(b)}
        sellEnabled(swSellActive.isChecked)
        swSellActive.setOnCheckedChangeListener { _, b ->  sellEnabled(b)}

        getBalance(symbol.toString(), decQty, price.toString())
        //getTrade(symbol.toString()){}

        var twHandler = object: TextWatcher {

            override fun afterTextChanged(p0: Editable?) {
                var sell = etPriceSell.text.toString()
                var stop = etSellStop.text.toString()
                var stopLimit = etSellStopLimit.text.toString()
                var price = etPriceLimit.text.toString()
                var perSell = etPerSell.text.toString()
                var perStop = etPerStop.text.toString()

                /*if (price.isNotEmpty() && price.toDouble() == p0.toString().toDouble()) {
                    etPriceLimit.removeTextChangedListener(this);
                    etPriceLimit.setText(Trade().formatDigits(price,digits))
                    etPriceLimit.addTextChangedListener(this);
                }
                if (sell.isNotEmpty() && sell.toDouble() == p0.toString().toDouble()) {
                    etPriceSell.removeTextChangedListener(this);
                    etPriceSell.setText(Trade().formatDigits(sell,digits))
                    etPriceSell.addTextChangedListener(this);
                }
                if (stop.isNotEmpty() && stop.toDouble() == p0.toString().toDouble()) {
                    etSellStop.removeTextChangedListener(this);
                    etSellStop.setText(Trade().formatDigits(stop,digits))
                    etSellStop.addTextChangedListener(this);
                }*/
                if (p0.toString().isNotEmpty() && price.isNotEmpty() && p0.toString() != "-") {
                    //Toast.makeText(applicationContext, p0.toString(), Toast.LENGTH_LONG).show()
                    when {
                        perSell.isNotEmpty() && perSell.toDouble() == p0.toString().toDouble() -> {
                            etPriceSell.removeTextChangedListener(this)
                            etPriceSell.setText(Trade().resultPer(etPriceLimit.text.toString().toDouble(),p0.toString().toDouble()))
                            Trade().setColorPer(etPerSell, sign1)
                            etPriceSell.addTextChangedListener(this)
                        }
                        perStop.isNotEmpty() && perStop.toDouble() == p0.toString().toDouble() -> {
                            etSellStopLimit.removeTextChangedListener(this)
                            var rp = Trade().resultPer(price.toDouble(),p0.toString().toDouble())
                            etSellStopLimit.setText(rp)
                            etSellStop.setText(rp)
                            Trade().setColorPer(etPerStop, sign2)
                            etSellStopLimit.addTextChangedListener(this)
                        }
                        stopLimit.isNotEmpty() && stopLimit.toDouble() == p0.toString().toDouble() -> {
                            //etSellStop.removeTextChangedListener(this);
                            etSellStop.setText(Trade().formatDigits(stopLimit,digits))
                            //etSellStop.addTextChangedListener(this);
                        }
                    }
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().isNotEmpty() && p0.toString() != "-") {
                    var sell = etPriceSell.text.toString()
                    var stop = etSellStop.text.toString()
                    var stopLimit = etSellStopLimit.text.toString()
                    var price = etPriceLimit.text.toString()
                    var sellTot = sell

                    when (spTypeOrderSell.selectedItemPosition) {
                        0 -> sellTot = sell
                        1 -> sellTot = stopLimit
                        3 -> sellTot = sell
                    }
                    if (price.isNotEmpty() && sell.isNotEmpty() && (p0.toString().toDouble() == price.toDouble() || p0.toString().toDouble() == sell.toDouble())) {
                        etPerSell.removeTextChangedListener(this)
                        etPerSell.setText(Trade().percentage(price.toDouble(), sell.toDouble()))
                        Trade().setColorPer(etPerSell, sign1)
                        etPerSell.addTextChangedListener(this)
                    }
                    if (price.isNotEmpty() && stopLimit.isNotEmpty()  && (p0.toString().toDouble() == price.toDouble() || p0.toString().toDouble() == stopLimit.toDouble())) {
                        etPerStop.removeTextChangedListener(this)
                        etPerStop.setText(Trade().percentage(price.toDouble(), stopLimit.toDouble()))
                        Trade().setColorPer(etPerStop, sign2)
                        etPerStop.addTextChangedListener(this)
                    }
                    if (etQtyBuy.text.toString().isNotEmpty() && price.isNotEmpty()) tvTotal.text = String.format("%.8f", (etQtyBuy.text.toString().toDouble() * price.toDouble()))
                    if (etQtySell.text.toString().isNotEmpty() && sellTot.isNotEmpty()) tvOrderTot.text = String.format("%.8f", (etQtySell.text.toString().toDouble() * sellTot.toDouble()))
                    //Toast.makeText(applicationContext, "cambio", Toast.LENGTH_SHORT).show()
                }
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        }

        etPriceLimit.addTextChangedListener(twHandler)
        etPriceSell.addTextChangedListener(twHandler)
        //etSellStop.addTextChangedListener(twHandler)
        etSellStopLimit.addTextChangedListener(twHandler)
        etQtyBuy.addTextChangedListener(twHandler)
        etQtySell.addTextChangedListener(twHandler)
        etPerSell.addTextChangedListener(twHandler)
        etPerStop.addTextChangedListener(twHandler)

        val repeatedHandler = Handler()
        val newRunnable = object : Runnable {
            private lateinit var v: View

            fun runnable(v: View) {
                this.v = v
            }
            override fun run() {
                var price = etPriceLimit.text.toString()
                var amountBuy = etQtyBuy.text.toString()
                var amountSell = etQtySell.text.toString()
                var sell = etPriceSell.text.toString()
                var stop = etSellStop.text.toString()
                var stop_limit = etSellStopLimit.text.toString()

                when (v.id) {
                    R.id.limit_plus_btn -> etPriceLimit.setText(Trade().plusPrice(price, digits))

                    R.id.limit_minus_btn -> etPriceLimit.setText(Trade().minusPrice(price, digits))

                    R.id.qty_buy_plus_btn -> etQtyBuy.setText(Trade().plusPrice(amountBuy, decQty))

                    R.id.qty_buy_minus_btn -> etQtyBuy.setText(Trade().minusPrice(amountBuy, decQty))

                    R.id.sell_plus_btn -> etPriceSell.setText(Trade().plusPrice(sell, digits))

                    R.id.sell_minus_btn -> etPriceSell.setText(Trade().minusPrice(sell, digits))

                    R.id.sell_stop_plus_btn -> etSellStop.setText(Trade().plusPrice(stop, digits))

                    R.id.sell_stop_minus_btn -> etSellStop.setText(Trade().minusPrice(stop, digits))

                    R.id.stop_limit_plus_btn -> etSellStopLimit.setText(Trade().plusPrice(stop_limit, digits))

                    R.id.stop_limit_minus_btn -> etSellStopLimit.setText(Trade().minusPrice(stop_limit, digits))

                    R.id.qty_sell_plus_btn -> etQtySell.setText(Trade().plusPrice(amountSell, decQty))

                    R.id.qty_sell_minus_btn -> etQtySell.setText(Trade().minusPrice(amountSell, decQty))
                }

                repeatedHandler.postDelayed(this, 100)
            }
        }

        var tHandler = View.OnTouchListener { view, motionEvent ->

            newRunnable.runnable(view)
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    repeatedHandler.postDelayed(newRunnable, 100)
                }
                MotionEvent.ACTION_UP -> {
                    repeatedHandler.removeCallbacks(newRunnable)
                }
            }
            true
        }

        /*var formatHandler = View.OnTouchListener{ view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    view.removeTextChangedListener()
                }
                MotionEvent.ACTION_UP -> {}
            }
        }*/

        limit_plus_btn.setOnTouchListener(tHandler)
        limit_minus_btn.setOnTouchListener(tHandler)
        qty_buy_plus_btn.setOnTouchListener(tHandler)
        qty_buy_minus_btn.setOnTouchListener(tHandler)
        sell_plus_btn.setOnTouchListener(tHandler)
        sell_minus_btn.setOnTouchListener(tHandler)
        sell_stop_plus_btn.setOnTouchListener(tHandler)
        sell_stop_minus_btn.setOnTouchListener(tHandler)
        stop_limit_plus_btn.setOnTouchListener(tHandler)
        stop_limit_minus_btn.setOnTouchListener(tHandler)
        qty_sell_plus_btn.setOnTouchListener(tHandler)
        qty_sell_minus_btn.setOnTouchListener(tHandler)

        try {
            mSocket = IO.socket("${RetrofitClient.url}:${RetrofitClient.port0}")
            mSocket.connect()
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        mSocket.emit("json", symbol.toString())

        mSocket.on("json_${symbol.toString()}") { args ->
            runOnUiThread(Runnable {
                val data = args[0] as JSONObject
                val lastPrice: String
                val percentChange: String
                try {
                    //result.add((1+i).toString())
                    lastPrice = data.getString("close")
                    percentChange = data.getString("percentChange")
                } catch (e: JSONException) {
                    return@Runnable
                }

                tvLastPrice.text = String.format("%.${digits}f", lastPrice.toDouble())
                tvPercentChange.text = String.format("%.2f", percentChange.toDouble())
                Trade().setColorPer(tvPercentChange, sign0)
                val network = Network(applicationContext, sharedPref)
                if (tvAvBal.text.toString().isNotEmpty() && symbol.toString().isNotEmpty()) {
                    network.trade(symbol.toString()){ result ->
                        if (tvAvBal.text.toString().toDouble() > 0 && result != null) {
                            lnPL.visibility = LinearLayout.VISIBLE
                            plPercent.text = Trade().percentage(etPriceLimit.text.toString().toDouble(), lastPrice.toDouble())
                            Trade().setColorPer(plPercent, plSign)
                        }
                    }
                }
            })
        }

        mSocket.on("${symbol.toString()}_filled") {args ->
            runOnUiThread(Runnable {
                val data = args[0] as JSONObject
                var side: String
                try {
                    side = data.getString("side");
                } catch (e: JSONException) {
                    return@Runnable
                }

                when (side) {
                    "buy" -> {
                        swBuyActive.isChecked = false
                    }
                    "sell" -> {
                        swBuyActive.isChecked = true
                        swSellActive.isChecked = true
                        lnPL.visibility = LinearLayout.GONE
                    }
                }

                getBalance(symbol.toString(), decQty, price.toString())
                Toast.makeText(applicationContext,"emitiendo filled", Toast.LENGTH_SHORT).show()
            })
        }

        mSocket.on("${symbol.toString()}_status") {args ->
            runOnUiThread(Runnable {
                val data = args[0] as JSONObject
                var status: String
                try {
                    status = data.getString("status")
                    getBalance(tvSymbol.text.toString(), decQty, etPriceLimit.text.toString())
                    when (status) {
                        "PENDING" -> btnCancel.visibility = LinearLayout.VISIBLE
                        "CANCELED" -> btnCancel.visibility = LinearLayout.GONE
                        "COMPLETED" -> {
                            btnCancel.visibility = LinearLayout.GONE
                            lnPL.visibility = LinearLayout.GONE
                        }
                    }
                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }

        mSocket.on("onOrder") {args ->
            runOnUiThread(Runnable {
                val data = args[0] as JSONArray
                try {
                    for (i in 0..data.length()) {
                        var obj = data.getJSONObject(i)
                        when (obj.has("type")) {
                            false -> {
                                etPriceLimit.setText(obj.getString("limit"))
                                etQtyBuy.setText(obj.getString("amount"))
                            }
                            true -> {
                                spTypeOrderSell.setSelection(obj.getInt("type"))
                                //etQtySell.setText(String.format("%.${decQty}", obj.getString("amount").toDouble()))
                                etQtySell.setText(obj.getString("amount"))
                                when (obj.getString("type")) {
                                    "0" -> {
                                        etPriceSell.setText(obj.getString("limit"))
                                    }
                                    "1" -> {
                                        etSellStopLimit.setText(obj.getString("stop_limit"))
                                        etSellStop.setText(obj.getString("stop"))
                                    }
                                    "2" -> {
                                        etPriceSell.setText(obj.getString("limit"))
                                        etSellStopLimit.setText(obj.getString("stop_limit"))
                                        etSellStop.setText(obj.getString("stop"))
                                    }
                                }
                            }
                        }
                    }
                    getBalance(symbol.toString(), decQty, price.toString())
                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }

        btnTrade.setOnClickListener{
            val rt = JSONObject(rulesTotal)
            var typeCond = spTypeOrderSell.selectedItemPosition == 2 || spTypeOrderSell.selectedItemPosition == 1
            var msg = "no order"
            if (swBuyActive.isChecked) {
                val minimum = rt.getString(tvCoin.text.toString())
                msg =
                    when {
                        (tvTotal.text.toString().toDouble() == 0.00 && swBuyActive.isChecked) || (tvOrderTot.text.toString().toDouble() == 0.00 && !swBuyActive.isChecked) -> "Price * QTY is zero"
                        tvTotal.text.toString().toDouble() < minimum.toDouble() && swBuyActive.isChecked -> "Total value must ve at least $minimum"
                        tvTotal.text.toString().toDouble() > tvBalance.text.toString().toDouble() -> "Insufficient balance"
                        else -> "success"
                    }

            }
            else if (swSellActive.isChecked) {
                val minimum = rt.getString((tvOrderCoin.text.toString()))
                msg = when {
                    tvOrderTot.text.toString().toDouble() == 0.00 -> "Price * QTY is zero"
                    tvOrderTot.text.toString().toDouble() < minimum.toDouble() -> "Total value must ve at least $minimum"
                    tvAvBal.text.toString().toDouble() < etQtySell.text.toString().toDouble() -> "Insufficient balance"
                    else -> "success"
                }
            }


            msg =
                if (etPriceLimit.text.toString().isNotEmpty() && etSellStop.text.toString().isNotEmpty() && etSellStopLimit.text.toString().isNotEmpty()) {
                    when {
                        typeCond && etSellStop.text.toString().toDouble() > etPriceLimit.text.toString().toDouble() -> "Sell Stop > Buy Limit"
                        typeCond && etSellStopLimit.text.toString().toDouble() > etPriceLimit.text.toString().toDouble() -> "Sell Stop Limit > Buy Limit"
                        else -> "success"
                    }
                } else "empty"

            if (etQtyBuy.text.toString().isEmpty() || etQtySell.text.toString().isEmpty()) msg = "QTY empty"

            if (msg.isNotEmpty() && msg != "success") Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
            if (msg == "success") {
                setOrder()
            }
        }

        btnCancel.setOnClickListener{
            network.orderCancel(symbol.toString()){}
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //mSocket.disconnect()
        mSocket.off ( "json")
        //Toast.makeText(applicationContext,"disconnect",Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        if (tvSymbol.text.toString().isNotEmpty()) {
            mSocket.emit("offTa")
            mSocket.off ( "json_${tvSymbol.text}")
        }
        //Toast.makeText(applicationContext,"pause",Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        if (tvSymbol.text.toString().isNotEmpty()) mSocket.emit("json", tvSymbol.text.toString())
        //Toast.makeText(applicationContext,"resume",Toast.LENGTH_SHORT).show()
    }

    private fun addLimitContent () {
        tvSellLimit.visibility = LinearLayout.VISIBLE
        tvSellStop.visibility = LinearLayout.GONE
        sell_limit.visibility = LinearLayout.VISIBLE
        sell_stop.visibility = LinearLayout.GONE
        sell_stop_limit.visibility = LinearLayout.GONE
    }

    private fun addStopContent () {
        tvSellLimit.visibility = LinearLayout.GONE
        tvSellStop.visibility = LinearLayout.VISIBLE
        sell_limit.visibility = LinearLayout.GONE
        sell_stop.visibility = LinearLayout.VISIBLE
        sell_stop_limit.visibility = LinearLayout.VISIBLE
    }

    private fun addOCOContent () {
        tvSellLimit.visibility = LinearLayout.VISIBLE
        tvSellStop.visibility = LinearLayout.VISIBLE
        sell_limit.visibility = LinearLayout.VISIBLE
        sell_stop.visibility = LinearLayout.VISIBLE
        sell_stop_limit.visibility = LinearLayout.VISIBLE
    }

    private fun setOrder () {
        var type = spTypeOrderSell.selectedItemPosition
        val token = sharedPref.getString("auth","").toString()
        RetrofitClient.instance.setOrder(
            token,
            tvSymbol.text.toString(),
            type,
            swBuyActive.isChecked,
            swSellActive.isChecked,
            etPriceLimit.text.toString(),
            etQtyBuy.text.toString(),
            etPriceSell.text.toString(),
            etSellStop.text.toString(),
            etSellStopLimit.text.toString(),
            etQtySell.text.toString())
            .enqueue(object: Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    Toast.makeText(applicationContext, response.body()!!.toString(), Toast.LENGTH_LONG).show()
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                }
            })
    }

    private fun getBalance (symbol: String, decQty: Int, price: String) {
        val network = Network(applicationContext, sharedPref)
        network.balance(symbol){ result ->
            var obb = result
            //btnCancel.visibility = LinearLayout.GONE
            //if (obb[0].onOrder!!.toDouble() > 0 || obb[1].onOrder!!.toDouble() > 0) btnCancel.visibility = LinearLayout.VISIBLE
            getTrade(symbol){ result ->

                balance =
                    if (obb[1].onOrder!!.toDouble() > 0 && result != null) String.format("%.8f",(obb[1].onOrder!!.toDouble() + obb[1]!!.available!!.toDouble()))
                    else obb[1].available!!.toString()

                tvAvBal.text = 
                    if (obb[0].onOrder!!.toDouble() > 0 && result != null) String.format("%.8f", (obb[0].onOrder!!.toDouble() + obb[0].available!!.toDouble()))
                    else obb[0].available

                tvCoin.text = obb[1].coin!!
                tvbCoin.text = tvCoin.text
                tvOrderCoin.text = obb[1].coin
                tvAvCoin.text = obb[0].coin
                var num = Trade().qtyCalc(price, balance, decQty)
                tvTotal.text = String.format("%.8f", (num.toDouble() * price.toDouble()))
                tvBalance.text = balance

                etQtyBuy.setText(num)
                etQtySell.setText(num)

                var s1 = obb[0].coin

                tvOrderTot.text = String.format("%.8f", (etQtySell.text.toString().toDouble() * price.toDouble()))
                if (tvAvBal.text.toString().toDouble() > 0) {
                    // Solo venta
                    swBuyActive.isChecked = false
                    etQtySell.setText(String.format("%.${decQty}f", tvAvBal.text.toString().toDouble()))
                }
            }

            /*if (obb[1].onOrder!!.toDouble() == 0.00)*/
            /*if (obb[1].onOrder!!.toDouble() == 0.00 && obb[0].onOrder!!.toDouble() == 0.00 )*/
            /*else {
                if (obb[1].onOrder!!.toDouble() == 0.00) etQtyBuy.setText(num)
                    etQtySell.setText(num) //Operacion de compra disponible
            }*/
            //Toast.makeText(applicationContext, s1, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getTrade (symbol: String, body: (Trade?) -> Unit) {
        val network = Network(applicationContext, sharedPref)
        network.trade(symbol){ result ->
            var res: Trade? = null
            if (result.body() != null) {
                btnCancel.visibility = LinearLayout.VISIBLE
                res = result.body()!!
                if (res.buy!!.active.isNotEmpty()) swBuyActive.isChecked = res.buy!!.active.toBoolean()
                if (res.buy!!.limit.isNotEmpty()) etPriceLimit.setText(res.buy!!.limit)
                if (res.buy!!.amount.isNotEmpty()) etQtyBuy.setText(res.buy!!.amount)

                if (res.sell!!.active.isNotEmpty()) swSellActive.isChecked = res.sell!!.active.toBoolean()
                if (res.sell!!.active.isNotEmpty() && res.sell!!.active.toBoolean()) {
                    spTypeOrderSell.setSelection(res.sell!!.type.toInt())
                    if (res.sell!!.limit.isNotEmpty()) etPriceSell.setText(res.sell!!.limit)
                    if (res.sell!!.stop.isNotEmpty()) etSellStop.setText(res.sell!!.stop)
                    if (res.sell!!.stopLimit.isNotEmpty()) etSellStopLimit.setText(res.sell!!.stopLimit)
                    if (res.sell!!.amount.isNotEmpty()) etQtySell.setText(res.sell!!.amount)
                }

            } else btnCancel.visibility = LinearLayout.GONE
            //Toast.makeText(applicationContext, result.symbol, Toast.LENGTH_LONG).show()
            //if (body != null) {
                body(res)
            //}
        }
    }

    private fun buyEnabled (b: Boolean) {
        when (b) {
            true -> {
                etPriceLimit.isEnabled = true
                etQtyBuy.isEnabled = true
                limit_minus_btn.isEnabled = true
                limit_plus_btn.isEnabled = true
                qty_buy_minus_btn.isEnabled = true
                qty_buy_plus_btn.isEnabled = true
            }

            false -> {
                etPriceLimit.isEnabled = false
                etQtyBuy.isEnabled = false
                limit_minus_btn.isEnabled = false
                limit_plus_btn.isEnabled = false
                qty_buy_minus_btn.isEnabled = false
                qty_buy_plus_btn.isEnabled = false
            }
        }
    }

    private fun sellEnabled (b: Boolean) {
        when (b) {
            true -> {
                spTypeOrderSell.isEnabled = true
                etPriceSell.isEnabled = true
                etQtySell.isEnabled = true
                etSellStop.isEnabled = true
                etSellStopLimit.isEnabled = true
                sell_minus_btn.isEnabled = true
                sell_plus_btn.isEnabled = true
                sell_stop_minus_btn.isEnabled = true
                sell_stop_plus_btn.isEnabled = true
                stop_limit_minus_btn.isEnabled = true
                stop_limit_plus_btn.isEnabled = true
                qty_sell_minus_btn.isEnabled = true
                qty_sell_plus_btn.isEnabled = true
            }

            false -> {
                spTypeOrderSell.isEnabled = false
                etPriceSell.isEnabled = false
                etQtySell.isEnabled = false
                etSellStop.isEnabled = false
                etSellStopLimit.isEnabled = false
                sell_minus_btn.isEnabled = false
                sell_plus_btn.isEnabled = false
                sell_stop_minus_btn.isEnabled = false
                sell_stop_plus_btn.isEnabled = false
                stop_limit_minus_btn.isEnabled = false
                stop_limit_plus_btn.isEnabled = false
                qty_sell_minus_btn.isEnabled = false
                qty_sell_plus_btn.isEnabled = false
            }
        }
    }
}
