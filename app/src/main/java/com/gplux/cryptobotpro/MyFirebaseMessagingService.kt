package com.gplux.cryptobotpro

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*


class MyFirebaseMessagingService: FirebaseMessagingService() {

    val tag = "NotificationTrading"
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val channelId = "TRADING"
        val data = remoteMessage!!.data
        val msg = remoteMessage!!.notification
        Log.d(tag, "From: " + remoteMessage!!.from)
        //Log.d(tag, "Notification Message Body: " + remoteMessage.notification!!.body!!)
        Log.d(tag,"Notification Data:" + remoteMessage!!.notification)

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(data["title"])
            .setContentText(data["body"])
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setLights(Color.RED, 3000,3000)
            .setContentIntent(pendingIntent)


        notificationBuilder.priority = NotificationCompat.PRIORITY_MAX
        notificationBuilder.setVibrate(longArrayOf(1000,1000,1000,1000,1000))
        notificationBuilder.setOngoing(true)
        notificationBuilder.setAutoCancel(true)
        notificationBuilder.setSound(defaultSoundUri)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "test",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableLights(true)
            notificationManager.createNotificationChannel(channel)
        }
        val idNot = Random().nextInt()
        notificationManager.notify(0,notificationBuilder.build())

    }

}